# Overbookd management

Repo for Overbookd management

## Pipeline status

|          | Dev | Preprod | Prod |
|----------|-----|---------|------|
| Backend  | ![](https://gitlab.com/24-heures-insa/overbookd/backend/badges/develop/pipeline.svg?key_text=develop+pipeleine&key_width=105) | ![](https://gitlab.com/24-heures-insa/overbookd/backend/badges/pre-prod/pipeline.svg?key_text=pre-prod+pipeleine&key_width=110) | ![](https://gitlab.com/24-heures-insa/overbookd/backend/badges/master/pipeline.svg?key_text=master+pipeleine&key_width=100) |
| Frontend | ![](https://gitlab.com/24-heures-insa/overbookd/frontend/badges/develop/pipeline.svg?key_text=develop+pipeleine&key_width=105) | ![](https://gitlab.com/24-heures-insa/overbookd/frontend/badges/pre-prod/pipeline.svg?key_text=pre-prod+pipeleine&key_width=110) | ![](https://gitlab.com/24-heures-insa/overbookd/frontend/badges/master/pipeline.svg?key_text=master+pipeleine&key_width=100) |

## Who has in this repo

First you can find docker compose for the prod, pre-prod and dev in ``docker`` repositorie
Second you find a configurator for config the API