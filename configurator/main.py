import requests
import json
from requests.structures import CaseInsensitiveDict
import argparse
import progressbar

URL = {
    "prod" : "https://overbookd.24heures.org/api/config",
    "pre-prod": "https://preprod.overbookd.24heures.org/api/config",
    "dev": "https://overbookd.traefik.me/api",
}

def configurator(token, url):
    headers = CaseInsensitiveDict()
    headers["Accept"] = "application/json"
    headers["Authorization"] = f"Bearer {token}"
    headers["Content-type"] = "application/json"

    with open("configs.json", "r", encoding='utf-8') as file:
        payload = json.load(file)
        for index in progressbar.progressbar(range(len(payload))):
            data = payload[index]
            correctData = {}
            correctData["key"] = data["key"]
            correctData["value"] = data["value"]
            response = requests.request("PUT", url, headers=headers, data=json.dumps([correctData]))

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--pre-prod", help="preprod")
    parser.add_argument("--prod", help="prod")
    parser.add_argument("--dev", "-d", help="dev")
    parser.add_argument("token", help="Bearer token")

    args = parser.parse_args()

    url = URL["dev"]
    if args.prod:
        url = URL["prod"]
    if args.pre_prod:
        url = URL["pre-prod"]

    configurator(token=args.token, url=url)
