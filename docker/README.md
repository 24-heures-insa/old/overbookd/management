# Docker for Overbookd

Here you have all files to deploy overbookd

Before all, for prod & pre-prod and dev compose, you should create a network :

```bash
docker network create traefik-public
```

## Prod and pre-prod

If you want to deploy in prod and pre-prod, check ``.env.example`` file and configure all Vars. To use automatically the
latest version and have domain. First start utils tools :
 - Watchtower (auto update)
 - Traefik (Revers proxy)

Traefik is configured to redirect automatically HTTPS and have not resolver configured

For that, run :
```bash
docker-compose -f docker-compose_utils.yml -p utils up -d
```
   
And you can run prod with : 

```bash
docker-compose -f docker-compose.yml -p prod up -d
```

And for pre-prod use : 

```bash
docker-compose -f docker-compose-preprod.yml up -d
```

## Dev

The dev docker-compose build automatically repo from ``../../frontend`` and ``../../backend``, so you should have this 
structure
```text
+ - gestion
|   + - docker
|   | + - assets
|   | + - data
|   | + .env
|   | ` docker-compose*.yml
|   + - configurator
+ - Backend
|   + - src
|   ` Dockerfile
` - Frontend
    + - src
    ` Dockerfile
```

And you can use this docker compose command to start
```bash
docker-compose -f docker-compose-dev.yml -p dev --env-file dev.env up
```

or use the provided script
```bash
./compose-dev.sh
```

Default user and password for Traefik dashboard is ``user`` : ``password`` 

Default URL for dev
--
| URL | Usage | Example |
|-----|-------|---------|
|traefik.${DOMAIN}| Traefik dashboard | https://traefik.traefik.me |
|overbookd.${DOMAIN}| Overbookd frontend | https://overbookd.traefik.me |
|overbookd.${DOMAIN}/api| Overbookd backend | https://overbookd.traefik.me/api |

Default URL for prod
--

| URL | Usage | Example |
|-----|-------|---------|
|traefik.${DOMAIN}| Traefik dashboard | https://traefik.localhost |
|overbookd.${DOMAIN}| Overbookd frontend | https://overbookd.localhost |
|overbookd.${DOMAIN}/api| Overbookd backend | https://overbookd.localhost/api |
|overbookd.${DOMAIN}/auth| Keycloak entry point | https://overbookd.localhost/auth |

For pre-prod
--

| URL | Usage | Example |
|-----|-------|---------|
|preprod.overbookd.${DOMAIN}| Overbookd frontend | https://preprod.overbookd.localhost |
|preprod.overbookd.${DOMAIN}/api| Overbookd backend | https://preprod.overbookd.localhost/api |
|preprod.overbookd.${DOMAIN}/auth| Keycloak entry point | https://preprod.overbookd.localhost/auth |

## Keyclaok admin user creation

Connect to keycloak with overbookd.${DOMAIN}/auth and connect with ``KEYCLOAK_USER`` and ``KEYCLOAK_PASSWORD`` from the ``.env`` file. Go to ``Users`` section and add a new user with ``OVERBOOKD_KEYCLOAK_ADMIN_USERNAME``and ``OVERBOOKD_KEYCLOAK_ADMIN_PASSWORD`` from the ``.env`` file. Take care to define a permanent password. To finish add in ``realm-management`` client roles the ``manage-users`` role

## The first user

You can register on the front page att [https://overbookd.traefik.me/signup](https://overbookd.traefik.me/signup)  
After you should use [Mongo compass](https://www.mongodb.com/try/download/compass) to access database with this URL : ``mongodb://overbookd:password@127.0.0.1:27017``  
To finish in user collection you can add ``admin`` role in team of your user :

```json
{
  "team": ["admin"]
}
```